#!/usr/bin/env python3

# ================== #
# Auteur : Breizh_29 #
# ================== #

# Import

# https://pypi.org/project/youtube_dl/
# https://mutagen.readthedocs.io
# https://docs.python.org/fr/3.9/library/os.html

import yt_dlp
from mutagen.id3 import ID3, TIT2, TALB, TPE1, TPE2, COMM, TCOM, TCON, TDRC, TRCK, APIC
from mutagen import File

import os
import sys

# For album cover
import requests
from PIL import Image
from io import BytesIO


# ========== FUNCTIONS ========== #
"""Filter for forbidden char in file name"""
FILTER = {
    '/': '_',
    '"': '\'',
    '?': '',
    ':': '_',
    '|': '_'
}

CALLBACKS = (None, None)

def dl_progress(e):
    try:
        status = e['status']
        if status == 'downloading' and CALLBACKS[0]:
            progress = e['downloaded_bytes'] / e['total_bytes']
            CALLBACKS[0](int(progress * 100))
        if status == 'finished' and CALLBACKS[1]:
            CALLBACKS[1]()
    except:
        pass

""" Option for Youtube dl """
OPTIONS = {
    'format': 'bestaudio/best', # quality choice
    'extractaudio' : True,      # audio only
    'postprocessors': [{        # in mp3
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '200',
    }],
    'quiet': True,              # remove youtube-dl output
    'verbose': False,
    'noprogress': True,
    'progress_hooks': [dl_progress]
}

""" Set music quality in options """
def setOptions(quality=200):
    OPTIONS['postprocessors'][0]['preferredquality'] = quality


""" Replace forbiden char in file name"""
def replace_filename(filename, associations):
    for key, value in associations.items():
        filename = filename.replace(key, value)
    return filename

def path_concat(*args):
    return os.sep.join(args)

""" Create folder for artist and album"""
def prepare_album_folders(artist, album):
    # === Artiste === #
    try: os.mkdir(artist)
    except: pass

    # === Album === #
    try: os.mkdir(path_concat(artist, album))
    except: pass


def set_progress_callbacks(downloading=None, finished=None):
    global CALLBACKS
    CALLBACKS = (downloading, finished)

""" --- Download cover --- """
def download_cover(thumbnail, album):
    r = requests.get(thumbnail['url'])
    height = thumbnail['height']
    width = thumbnail['width']
    margin = (width - height)/2

    # if cover is not download
    if r.status_code != 200:
        return None

    # resize of cover
    image = Image.open(BytesIO(r.content))

    resized_image = image.crop((margin, 0, height+margin, height))
    resized_image = resized_image.resize((height,height))

    output = BytesIO(b'')
    resized_image.save(output, format='PNG')

    return output.getvalue()

"""
--- get info of playlist ---
Take list of videos
Take year of album
Take url cover of playlist but if the playlist is not referenced take url cover of firt element on playlist
"""
def get_pl_info(url):
    ydl = yt_dlp.YoutubeDL(OPTIONS)
    playlist_dict = ydl.extract_info(url, download=False, ie_key="YoutubeTab", process=False) #recuperation des infos
    videos = list(playlist_dict['entries'])

    if playlist_dict['thumbnails'] != []:
        thumbnail = playlist_dict['thumbnails'][-1]
    else :
        thumbnail = videos[1]['thumbnails'][-1]

    video_info = ydl.extract_info(videos[0].get('url'), ie_key="Youtube")
    # -- years --
    if video_info.get('release_year') != None:
        year = video_info.get('release_year')
    else:
        year = 0

    return (videos, year, thumbnail)

"""
Download and tags audio files
we tage: Artist, Track title, Album and track number
if possible, we tage: year, kind and cover
"""
def dl_and_tag_video(url, index, title, artist, album, year, album_art, genre=None, folder=True):

    # print(os.getcwd())

    basename = path_concat(artist, album, title)
    ytdl_template = basename + '.%(ext)s'
    options = OPTIONS
    options['outtmpl'] = ytdl_template
    # print(options)
    ydl = yt_dlp.YoutubeDL(OPTIONS)
    video = ydl.extract_info(url, ie_key="Youtube")

    # === Tags of audio files ===
    music = ID3(basename + '.mp3')

    # Artist
    music["TPE1"] = TPE1(encoding=3, text=artist)
    # TrackTitle
    music["TIT2"] = TIT2(encoding=3, text=title)
    # Album
    music["TALB"] = TALB(encoding=3, text=album)
    # Track Number
    music["TRCK"] = TRCK(encoding=3, text=index)
    # Year
    if year != 0:
        music["TDRC"] = TDRC(encoding=3, text=str(year))
    # kind
    if genre != None:
        music["TCON"] = TCON(encoding=3, text=genre)
    # cover
    if album_art != None:
        music["APIC"] = APIC(
            encoding=3,
            mime='image/png',
            type=3, desc=u'Cover',
            data=album_art
        )

    music.save()
