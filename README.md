# Musique-dl

Script qui permet de télécherger des album via une playlist youtube avec les meta donnée de la musique plus le renomage des fichier par le titre de la musique.

Les tags pris en compte sont :
- L'artiste
- Le nom de l'album
- Le nom de la piste
- Le numero de la piste
- Le nombre de piste de l'album
- L'année de sortie de l'album
- La pochette d'album
- Le genre (optionnel)


**music_dl** est le wraper de music_lib pour la ligne de commande

Au plus possible, les tags sont recuperer au  plus possible grâce aux information de la playlist youtube.

Durant le programme, si ils n'existe pas, un dossier pour l'artiste et pour l'album est crée.

## Interface en ligne de commande music_dl
Ils vous sera demandé le nom de l'artist, de l'album et l'url de la playlist

```console
usage: cmd-line [-h] [-a ARTIST] [-d DISK] [-u URL] [-g GENRE]
                [-q QUALITY] [-t]

options:
  -h, --help            show this help message and exit
  -a ARTIST, --artist ARTIST
                        Name of artist
  -d DISK, --disk DISK  Name of album
  -u URL, --url URL     Link of youtube playlist
  -g GENRE, --genre GENRE
                        Musical genre of album
  -q QUALITY, --quality QUALITY
                        Quality of musique in kbps
  -t, --test            Test with default value
```


## Library necessaire

yt_dlp, mutagen, os et PIL pour python
webgate pour l'interface graphique en rust
```console
pip install requests yt_dlp mutagen Pillow
```
ou avec pipenv
```console
pipenv sync
```
